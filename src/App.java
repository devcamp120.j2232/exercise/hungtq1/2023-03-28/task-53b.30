import models.Customer;
import models.Invoice;

public class App {
    public static void main(String[] args) throws Exception {
        Customer customer1 = new Customer(1, "Nguyen Van A", 50);
        Customer customer2 = new Customer(2, "Lai Thi B", 30);
        System.out.println("Customer 1: " + customer1.toString());
        System.out.println("Customer 2: " + customer2.toString());

        Invoice invoice1 = new Invoice(1, customer1, 100000);
        Invoice invoice2 = new Invoice(2, customer2, 120000);
        System.out.println("Invoice 1: " + invoice1.toString());
        System.out.println("Invoice 2: " + invoice2.toString());
    }
}
